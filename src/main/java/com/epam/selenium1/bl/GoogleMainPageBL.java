package com.epam.selenium1.bl;

import com.epam.selenium1.po.GoogleMainPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class GoogleMainPageBL {

  private GoogleMainPage googleMainPage;
  private static Logger logger = LogManager.getLogger(GoogleMainPageBL.class);

  public GoogleMainPageBL() {
    googleMainPage = new GoogleMainPage();
  }

  public void search(String query) {
    logger.info(String.format("Entering '%s' into the form...", query));
    googleMainPage.getForm().sendKeys(query);
    logger.info("Pressing submit button...");
    googleMainPage.getForm().submit();
  }
}
