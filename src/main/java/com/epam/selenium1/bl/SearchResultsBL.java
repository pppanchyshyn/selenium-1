package com.epam.selenium1.bl;

import com.epam.selenium1.po.SearchResults;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class SearchResultsBL {

  private SearchResults searchResults;
  private static Logger logger = LogManager.getLogger(SearchResultsBL.class);

  public SearchResultsBL() {
    searchResults = new SearchResults();
  }

  public void getImages() {
    logger.info("Clicking on image table...");
    searchResults.getImagesTabLink().click();
  }

  public boolean isImagesTabOpen() {
    logger.info("Checking whether the image table is open...");
    return Boolean.valueOf(searchResults.getImagesTab().getAttribute("aria-selected"));
  }
}
