package com.epam.selenium1.po;

import com.epam.selenium1.utils.WebDriverUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

abstract class AbstractPageObject {

  private WebDriver driver;

  AbstractPageObject() {
    this.driver = WebDriverUtils.getDriver();
    PageFactory.initElements(driver, this);
  }
}
