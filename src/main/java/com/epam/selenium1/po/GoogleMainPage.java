package com.epam.selenium1.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleMainPage extends AbstractPageObject{

  @FindBy(name = "q")
  private WebElement form;

  public WebElement getForm() {
    return form;
  }
}
