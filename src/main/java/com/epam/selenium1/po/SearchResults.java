package com.epam.selenium1.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResults extends AbstractPageObject {

  @FindBy(xpath = "//div[@role='tab'][2]/a")
  private WebElement imagesTabLink;

  @FindBy(xpath = "//div[@role='tab'][2]")
  private WebElement imagesTab;

  public WebElement getImagesTabLink() {
    return imagesTabLink;
  }

  public WebElement getImagesTab() {
    return imagesTab;
  }
}
