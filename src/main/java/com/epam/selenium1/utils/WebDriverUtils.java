package com.epam.selenium1.utils;

import java.util.Objects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverUtils {

  private static WebDriver driver;

  public static WebDriver getDriver() {
    if (Objects.isNull(driver)) {
      driver = new ChromeDriver();
    }
    return driver;
  }
}
