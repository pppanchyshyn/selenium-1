package com.epam.selenium1;

import com.epam.selenium1.bl.GoogleMainPageBL;
import com.epam.selenium1.bl.SearchResultsBL;
import com.epam.selenium1.utils.WebDriverUtils;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestGoogleSearch {

  private WebDriver driver;
  private GoogleMainPageBL googleMainPageBL;
  private SearchResultsBL searchResultsBL;

  @BeforeClass
  public void setUp() {
    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    driver = WebDriverUtils.getDriver();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @BeforeMethod
  public void initializeBLObjects() {
    googleMainPageBL = new GoogleMainPageBL();
    searchResultsBL = new SearchResultsBL();
  }

  @Test
  public void checkUserAbilityToViewGoogleImagesTest() {
    driver.get("https://www.google.com.ua");
    googleMainPageBL.search("Apple");
    Assert.assertTrue(driver.getTitle().contains("Apple"),
        "Page title does not contain the searched word ");
    searchResultsBL.getImages();
    Assert.assertTrue(searchResultsBL.isImagesTabOpen(), "The image table is not open");
  }

  @AfterMethod
  public void cleanUp() {
    driver.manage().deleteAllCookies();
  }

  @AfterClass
  public void tearDown() {
    driver.quit();
  }
}
